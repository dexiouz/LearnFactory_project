// let myArray = new Array() this is an old method of array declaration.
// Array counts its elements from zero.
 
 //there are 4 elements counting from zero. Net talks about the size or number of elements in that array
 //let myArray = [1,2,3,4,5]
 //alert(myArray[0]);

// let arr =[];
// arr[0] ="dog";
// arr[1] = "wealth";
// arr[2] = "prosper";
// arr[4] = 

// alert(arr.length);

// let fruits = [];
// fruits[100]= 'mango';
// alert(fruits);
// fruits[100]='apple';
// fruits[0]='orange';
// alert(fruits);


let animals =["goat", "dog", "sheep", "rabbit"];
alert(animals);

//shift
//shift removes an element from the beginning
// Works in 3 ways removes element from index 1 cause array to readjust element in it to assume a new index value, this the pc memory busy, instead use pop.


// animals.shift();
// alert(animals);

//pop
//pop: it removes the last, once it removes, the array remains the way it is. doesnt affect memory. Best to use pop.


// animals.pop();
// alert(animals);

//push :adding an element to the end of an array
// animals.push("banana","avocaado");
// alert(animals);



//unshift adds from the beginning, each of the element has to readjuc=st its index.

// animals.unshift("elephant");
// alert(animals);

// let myObj{
//     name ="CHI";
//     age =123;
//     tribe = "ibo";
// }
// alert(myObj.name);




for (let man of animals){
    alert(man);
}
