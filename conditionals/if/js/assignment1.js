//Rewrite using if ...else statement
// switch(browser){
//     case 'Edge':
//     alert("You've got the edge!");
//     break;

//     case 'Firefox':
//     case 'Chrome':
//     case 'Safari':
//     case 'Opera':
//     alert("Okay we support these too!");
//     break;

//     default:
//     alert("Okay we support these too");
// }


//If else statement
let browser = prompt("Which browser");
    if(browser=="edge"){
        alert("You selected " + browser +", You've got the " + browser + "!");
    }
    else if(browser=="firefox"||
        browser=="chrome"||
        browser=="safari"||
        browser=="opera"){
            alert("You selected " + browser + ", Okay we support these too");
        }
    else{
        alert("You selected " + browser + ", We hope that this site looks okay");
    }




//Rewrite using switch case
// let a = +prompt('a?', '');
// if (a == 0) {
//   alert( 0 );
// }
// if (a == 1) {
//   alert( 1 );
// }
// if (a == 2 || a == 3) {
//   alert( '2,3' );
// }


let a = +prompt("a?",'');
switch(a){
    case 0:
        alert("0");
        break;
    case 1:
        alert("1");
        break; 
    case 2:
    case 3:
        alert("2,3");
        break;
    default:
        alert("This is exceptional case");   
}